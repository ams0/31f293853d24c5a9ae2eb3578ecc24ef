#!/bin/bash

#brews
brew install wget
brew install mc
brew install tmux
brew install go
brew install node
brew install bash-completion
brew install m-cli
brew install tmux
brew install azure-cli
brew install kubernetes-cli
brew install kubernetes-helm
brew tap atombender/ktail
brew install atombender/ktail/ktail

#casks
brew tap caskroom/cask
brew cask install iina
brew cask install iterm2
brew cask install night-owl
brew cask install dotnet
brew cask install dotnet-sdk
brew cask install google-chrome
brew cask install visual-studio-code
brew cask install alfred
brew cask install fantastical
brew cask install telegram-desktop
brew cask install whatsapp
brew cask install dash
brew cask install evernote
brew cask install growl-fork
brew cask install firefox
brew cask install cyberduck
brew cask install mountain-duck
brew cask install teamviewer
brew cask install the-unarchiver
brew cask install virtualbox
brew cask install dropbox
brew cask install bartender
brew cask install postman
brew cask install vagrant
brew cask install cakebrew
brew cask install clipy
brew cask install yacreader
brew cask install rescuetime
brew cask install visual-studio
brew cask install spotify
brew cask install spotify-notifications
brew cask install lastpass
brew cask install docker
brew cask install caffeine
brew cask install messenger
brew cask install skype
brew cask install mattermost
brew cask install clipgrab
brew cask install slack
brew cask install fluid
brew cask install vlc
brew cask install mammon
brew cask install airserver
brew cask install transmission
brew cask install macdown
brew cask install forklift
brew cask install istat-menus
brew cask install royal-tsx
brew cask install netnewswire
brew cask install shazam
brew cask search onedrive
brew cask install microsoft-teams
brew cask install skype-for-business
brew cask install microsoft-azure-storage-explorer
brew cask install skitch

brew cask install qlstephen
brew cask install qlmarkdown
brew cask install quicklook-json
brew cask install quicklook-csv



# brew cleanup --force
# rm -f -r /Library/Caches/Homebrew/*

#not in brew but appstore
# iHosts
# snippetslab
# hardwaregrowler
# remote desktop
# hackernews

# download no appstore
# yammer
